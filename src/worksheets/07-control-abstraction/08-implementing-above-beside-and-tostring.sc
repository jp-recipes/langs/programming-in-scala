abstract class Element:
  def contents: Vector[String]

  val height = contents.length
  val width = if height == 0 then 0 else contents(0).length

  // Putting one element above another means concatenating the two 'contents' values of the elements
  def above(that: Element): Element =
    VectorElement(this.contents ++ that.contents)

  // Putting two elements beside each other
  def beside(that: Element): Element =
    VectorElement(
      for (line1, line2) <- this.contents.zip(that.contents)
        yield line + line2
    )

class VectorElement(val contents: Vector[String]) extends Element:
  override def toString = contents.mkString("\n")

/*
 Imperative Style

  def beside(that: Element): Element =
    val newContents = new Array[String](this.contents.length)
    for i <- this.contents.indices do
      newContents(i) = this.contents(i) + that.contents
    VectorElement(newContents.toVector)
*/