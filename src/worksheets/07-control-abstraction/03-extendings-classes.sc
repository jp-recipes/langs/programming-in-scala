abstract class Element:
  def contents: Vector[String]

  val height = contents.length
  val width = if height == 0 then 0 else contents(0).length

// Extending a class
class VectorElement(conts: Vector[String]) extends Element:
  def contents: Vector[String] = conts

val ve = VectorElement(Vector("Hello", "World"))
ve.width