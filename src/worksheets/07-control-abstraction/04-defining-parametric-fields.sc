abstract class Element:
  def contents: Vector[String]

  val height = contents.length
  val width = if height == 0 then 0 else contents(0).length

// Extending a class
class VectorElement(
  val contents: Vector[String]
) extends Element:
  