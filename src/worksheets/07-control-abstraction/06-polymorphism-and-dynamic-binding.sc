abstract class Element:
  def contents: Vector[String]

  val height = contents.length
  val width = if height == 0 then 0 else contents(0).length

class UniformElement(
  ch: Char,
  override val width: Int,
  override val height: Int
) extends Element:
  private val line = ch.toString * width

  override def contents = Vector.fill(height)(line)