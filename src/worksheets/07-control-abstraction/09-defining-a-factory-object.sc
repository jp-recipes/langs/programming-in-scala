object Element:
  // VectorElement
  private class VectorElement(val contents: Vector[String]) extends Element:
    override def toString = contents.mkString("\n")

  // LineElement
  private class LineElement(s: String) extends Element:
    val contents = Vector(s)

    override def width = s.length
    override def height = 1

  // UniformElement
  private class UniformElement(ch: Char, override val width: Int, override val height: Int) extends Element:
    private val line = ch.toString * width

    override def contents = Vector.fill(height)(line)

  def elem(contents: Vector[String]): Element =
    VectorElement(contents)

  def elem(chr: Char, width: Int, height: Int): Element =
    UniformElement(chr, width, height)

  def elem(line: String): Element =
    LineElement(line)

abstract class Element:
  def contents: Vector[String]

  val height = contents.length
  val width = if height == 0 then 0 else contents(0).length

  def above(that: Element): Element =
    elem(this.contents ++ that.contents)

  def beside(that: Element): Element =
    elem(
      for (line1, line2) <- this.contents.zip(that.contents)
        yield line1 + line2
    )
