abstract class Element:
  def contents: Vector[String]

  // Parameterless methods
  def height: Int = contents.length
  def width: Int = if height == 0 then 0 else contents(0).length
