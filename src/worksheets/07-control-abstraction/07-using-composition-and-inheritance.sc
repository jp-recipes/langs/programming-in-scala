abstract class Element:
  def contents: Vector[String]

  val height = contents.length
  val width = if height == 0 then 0 else contents(0).length


class LineElement(s: String) extends Element:
  val contents = Vector(s)
  override def width = s.length
  override def height = 1