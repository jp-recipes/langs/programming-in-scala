// Iteration through collections
val filesHere = (new java.io.File(".")).listFiles

// Generator
for file <- filesHere do
  println(file)

// Not common in Scala
for i <- 0 to filesHere.length - 1 do
  println(file)

// Ranges
for i <- 1 to 4 do
  println(s"Iteration $i")
// Iteration 1
// Iteration 2
// Iteration 3
// Iteration 4

for i <- 1 until 4 do
  println(s"Iteration $i")
// Iteration 1
// Iteration 2
// Iteration 3

// Filtering
for file <- filesHere if file.getName.endsWith(".scala") do
  println(file)

for file <- filesHere do
  if file.getName.endsWith(".scala") then
    println(file)

for
  file <- filesHere
  if file.isFile
  if file.getName.endsWith(".scala")
do println(file)

// Nested Iteration
def fileLines(file: java.io.File) =
  scala.io.Source.fromFile(file).getLines().toArray

def grep(pattern: String) =
  for
    file <- filesHere
    if file.getName.endsWith(".scala")
    line <- fileLines(file)
    if line.trim.matches(pattern)
  do println(s"$file: ${line.trim}")

grep(".*gcd.*")

