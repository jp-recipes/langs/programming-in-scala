extension (x: Int)
  def + (y: Rational) = Rational(x) + y
  def - (y: Rational) = Rational(x) - y
  def * (y: Rational) = Rational(x) * y
  def / (y: Rational) = Rational(x) / y
