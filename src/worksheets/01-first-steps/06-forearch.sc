// Inferred
def m(args: String*) =
  args.foreach(arg => println(arg))

// Explicit
def m1(args: String*) =
  args.foreach((arg: String) => println(arg))

// Even more concise
def m2(args: String*) =
  args.foreach(println)
