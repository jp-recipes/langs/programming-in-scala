// As of Scala 3, the indentation-based style, "quit syntax", is recommended over the curly brace style

def m(args: String*) =
  var i = 0
  while i < args.length do
    println(args(i))
    i += 1