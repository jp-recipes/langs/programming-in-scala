def max(x: Int, y: Int): Int =
  if x > y then x
  else y

// One statement single line
def max2(x: Int, y: Int): Int = if x > y then x else y

// Inferred return type
def max3(x: Int, y: Int) = if x > y then x else y

val bigger = max(3, 5)

// Implicit Unit return type
def greet() = print("Hello, World!")

greet()