
// Case classes can generate implementations of several methods based on the values passed to its primary constructor
// The compiler will generate an implementation of hashCode and equals for our class

case class Person(name: String, age: Int)

val a = Person("Sally", 39)
val b = Person("James", 40)

print(a.hashCode == b.hashCode)
print(a == b)
print(a.equals(b))

