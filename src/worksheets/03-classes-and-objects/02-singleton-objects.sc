import scala.collection.mutable

// When a singleton object shares the same name with a class, it is called that class's ´companion object´
// The class is called the ´companing class´ of the singleton object
//
// A class and its companion object can access each other's private members

// Companion Class
class ChecksumAccumulator:
  private var sum = 0

  def add(b: Byte): Unit = sum += b
  def checksum(): Int = ~(sum & 0xFF) + 1

// Companion Object
object ChecksumAccumulator:
  private val cache = mutable.Map.empty[String, Int]

  def calculate(s: String): Int =
    if cache.contains(s) then
      cache(s)
    else
      val acc = new ChecksumAccumulator
      for c <- s do
        acc.add((c >> 8).toByte)
        acc.add(c.toByte)
      val cs = acc.checksum()
      cache += (s -> cs)
      cs

main.ChecksumAccumulator.calculate("Every value is an object.")





