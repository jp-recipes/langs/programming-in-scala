val adjectives = List("One", "Two", "Red", "Blue")

val nouns = adjectives.map(adj => adj + " Fish")