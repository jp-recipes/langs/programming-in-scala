// List is an immutable array
val onTwoThree = List(1, 2, 3)

// List concatenation
val oneTwo = List(1, 2)
val threeFour = List(3, 4)
val oneTwoThreeFour = oneTwo ::: threeFour

// Prepends a new element to the beginning of an existing list and returns the resulting list
val twoThree = List(3, 4)
val twoThreeFour = 2 :: twoThree

// Begin with an empty list using Nil
val threeFourFive = 3 :: 2 :: 3 :: Nil

// Append (Not recommended. We can prepend and the reverse)
val fourFive = List(4, 5)
val fourFiveSix = fourFive :+ 6
