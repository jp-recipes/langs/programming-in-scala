import scala.collection.mutable
import scala.collection.immutable

// Mutable Set
var jetSet = mutable.Set("Boeing", "Airbus")
jetSet += "Lear"
val query = jetSet.contains("Cessna")

// Immutable Set
val movieSet = immutable.Set("Cessna")

