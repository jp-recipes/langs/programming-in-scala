val ques = Vector("Who", "What", "When", "Where", "Why")

ques
  .find(q => q.startsWith("W"))        // Some(Who)
  .map(word => word.toUpperCase)       // Some(WHO)

val who = ques
  .find(q => q.startsWith("W"))        // Some(Who)

for word <- who yield word.toUpperCase // Some(Who)


