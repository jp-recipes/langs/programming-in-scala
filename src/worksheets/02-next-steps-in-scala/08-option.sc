val ques = Vector("Who", "What", "When", "Where", "Why")

val usingMap = ques.map(q => q.toLowerCase + "?")
// Vector(who?, what?, when?, where?, why?)

val usingForYield =
  for q <- ques yield
      q.toLowerCase + "?"

val startsW = ques.find(q => q.startsWith("W"))  // Some(Who)
val startsH = ques.find(q => q.startsWith("H")) // None