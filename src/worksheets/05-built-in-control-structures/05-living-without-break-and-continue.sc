def searchWithWhile(args: String*) =
  var i = 0
  var foundIt = false
  while i < args.length && !foundIt do
    if !args(i).startsWith("-") then
      if args(i).endsWith(".scala") then
        foundIt = true
      else
        i += 1
    else
      i += 1


// Recursive

var args = Array[String]()

def searchFrom(i: Int): Int =
  if i >= args.length then -1
  else if args(i).startsWith("-") then searchFrom(i + 1)
  else if args(i).endsWith(".scala") then i
  else searchFrom(i + 1)

val i = searchFrom(0)
