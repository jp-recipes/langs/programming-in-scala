import java.io.FileReader

val file = new FileReader("input.text")

try
  println(file.read())
finally
  file.close()


// Finally overriding previous returned value
// Avoid returning values from finally

// Returns 2
def f(): Int = try return 1 finally return 2

// Returns 1
def g(): Int = try 1 finally 2

