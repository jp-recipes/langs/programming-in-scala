def m(args: String*): String =
  val firstArg = if !args.isEmpty then args(0) else ""
  firstArg match
    case "salt" => "pepper"
    case "chips" => "salsa"
    case "eggs" => "bacon"
    case _ => "huh?"

