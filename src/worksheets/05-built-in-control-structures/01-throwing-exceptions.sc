def half(n: Int) =
  if n % 2 == 0 then
    n / 2
  else
    throw new RuntimeException("n must be even")