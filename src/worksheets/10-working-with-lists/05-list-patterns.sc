// List can also be taken apart using pattern matching

val fruits = List("apples", "oranges", "pears")

// This pattern matches a List of 3 elements
val List(a, b, c) = fruits

// If we don't know the length of the list is better to use the next pattern
val a :: b :: rest = fruits
val aString: String = a
val bString: String = b
val restList: List[String] = rest