// List are immutable
// List hava a recursive structure (a linked list)

val fruit = List("apples", "oranges", "pears")
val nums = List(1, 2, 3, 4)
val diag =
  List(
    List(1, 0, 0),
    List(0, 1, 0),
    List(0, 0, 1)
  )
val empty = List()

