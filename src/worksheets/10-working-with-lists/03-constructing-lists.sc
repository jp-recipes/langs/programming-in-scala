// All lists are built from two fundamental building blocks, Nil and ::
// Nil represents an empty list
// The infix operator, ::, expresses a list extension at the front

val fruits = "apples" :: ("oranges" :: ("pears" :: Nil))
val nums = 1 :: (2 :: (3 :: (4 :: Nil)))
val diag = (1 :: (0 :: (0 :: Nil))) ::
           (0 :: (1 :: (0 :: Nil))) ::
           (0 :: (0 :: (1 :: Nil))) :: Nil
val empty = Nil