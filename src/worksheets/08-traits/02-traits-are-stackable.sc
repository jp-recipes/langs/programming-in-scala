import scala.collection.mutable.ArrayBuffer

abstract class IntQueue:
  def get(): Int
  def put(x: Int): Unit

class BasicIntQueue extends IntQueue:
  private val buf = ArrayBuffer.empty[Int]
  def get() = buf.remove(0)
  def put(x: Int): Unit = buf += x

val iqueue = new BasicIntQueue
iqueue.put(10)
iqueue.get() // 10

// Stackable modifications

trait Doubling extends IntQueue:
  abstract override def put(x: Int) = super.put(2 * x)

class MyQueue extends BasicIntQueue, Doubling

val queue = new MyQueue
queue.put(10)
queue.get() // 20

// Traits are dynamically bound

val tiqueue = new BasicIntQueue with Doubling
tiqueue.put(10)
tiqueue.get() // 20

// More traits

trait Incrementing extends IntQueue:
  abstract override def put(x: Int) = super.put(x + 1)

trait Filtering extends IntQueue:
  abstract override def put(x: Int) =
    if x >= 0 then super.put(x)

val mqueue = new BasicIntQueue with Incrementing with Filtering
mqueue.put(-1)
mqueue.put(0)
mqueue.put(1)
mqueue.get()
mqueue.get()
