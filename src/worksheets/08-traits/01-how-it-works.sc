// Definition
trait Philosophical:
  def philosophize = "I consume memory, therefore I am!"

// Once a trait is defined, it can be 'mixed in' to a class
class Toad extends Philosophical:
  override def philosophize: String = "green"

val frog = new Toad
frog.philosophize

// Mixing in multiple traits
class Animal
trait HasLegs

class Frog extends Animal, Philosophical, HasLegs:
  override def toString = "green"

  override def philosophize: String = s"It ain't easy being $this"

