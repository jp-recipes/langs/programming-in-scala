// Sealed classes cannot have any new subclasses added except the ones in the same file

sealed trait Expr
case class Var(name: String) extends Expr
case class Num(number: Double) extends Expr
case class UnOp(operator: String, arg: Expr) extends Expr
case class BinOp(operator: String, left: Expr, right: Expr) extends Expr

// Now define a pattern match where some of the possible cases are left out

def describe(e: Expr): String =
  e match // This will throw a MatchError exception
    case Num(_) => "a number"
    case Var(_) => "a variable"

// An alternative to MatchError

def describeThrowingError(e: Expr): String =
  e match
    case Num(_) => "a number"
    case Var(_) => "a variable"
    case _ => throw new RuntimeException

// @unchecked annotation
// If a match's selector expression carries this annotation, exhaustivity checking for the patterns that follow will be suppressed

def describeUnchecked(e: Expr): String =
  (e: @unchecked) match
    case Num(_) => "a number"
    case Var(_) => "a variable"