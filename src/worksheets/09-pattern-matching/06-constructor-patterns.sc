val expr: Expr = BinOp("+", Num(1), Num(0))

expr match
  case BinOp("+", e, Num(0)) => "a deep match"
  case _ => ""