// First, match is an expression in Scala (i.e., it always results in a value)
// Second, Scala's alternative expressions never "fall through" into the next case
// Third, if non of the patterns match, an exception names MatchError is thrown

def simplifyTop(expr: Expr): Expr =
  expr match
    case UnOp("-", UnOp("-", e)) => e
    case BinOp("+", e, Num(0)) => e
    case BinOp("*", e, Num(1)) => e
    case _ => expr
