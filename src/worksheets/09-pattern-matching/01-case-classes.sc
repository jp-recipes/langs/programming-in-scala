// Let's say we need to write a library that manipulates arithmetic expressions
// A first step to tackling this problem is the definition of the input data

trait Expr
case class Var(name: String) extends Expr
case class Num(number: Double) extends Expr
case class UnOp(operator: String, arg: Expr) extends Expr
case class BinOp(operator: String, left: Expr, right: Expr) extends Expr

// Case Classes
// Using the case modifier makes the Scala compiler add some syntactic conveniences to our class

// First, it adds a factory method with the name of the class

val v = Var("x")

// Factory methods are particularly nice when you nest them (no need for 'new' keyword)

val op = BinOp("+", Num(1), v)

// The second syntactic convenience is that all arguments in the parameter list of a case class implicitly get a val prefix

v.name
op.left

// Third, the compiler adds "natural" implementations of methods toString, hashCode, and equals to our class

op.toString
op.right == Var("x")

// Finally, the compiler adds a copy method to our class for making modified copies

op.copy(operator = "-")


