val xs = List(0, 2, 4)

// Exactly three elements with the first being 0
xs match
  case List(0, _, _) => "found it"
  case _ => ""

// N Elements with the first being 0
xs match
  case List(0, _*) => "found it"
  case _ => ""