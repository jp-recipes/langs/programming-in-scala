val expr: Expr = UnOp("abs", UnOp("abs", Num(3)))

expr match
  case UnOp("abs", e @ UnOp("abs", _)) => e
  case _ => ""

