val expr: Any = ":)"

expr match
  case 0 => "zero"
  case somethingElse => s"not zero $somethingElse"