// The wildcard pattern (_) matches any object whatsoever
val expr: Expr = BinOp("+", UnOp("+", Num(1)), UnOp("+", Num(1)))

expr match
  case BinOp(op, left, right) => s"$expr is a binary operation"
  case _ => s"It's something else"


expr match
  case BinOp(_, _, _) => s"$expr is a binary operation"
  case _ => s"It's something else"
