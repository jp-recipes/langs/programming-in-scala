def isGoodEnough(d: Double) = true
def improve(d: Double) = 1

// Recursive
def aproximate(guess: Double): Double =
  if isGoodEnough(guess) then guess
  else aproximate(improve(guess))

// Looping
def approximateLoop(initialGuess: Double): Double =
  var guess = initialGuess
  while !isGoodEnough(guess) do
    guess = improve(guess)
  guess