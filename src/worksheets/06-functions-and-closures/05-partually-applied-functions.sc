def sum(a: Int, b: Int, c: Int) = a + b + c

sum(1, 2, 3) // 6

val a1 = sum _
val a2 = sum(_, _, _)
val a3: (Int, Int, Int) => Int = sum(_, _, _)

val b1 = sum(1, _, 3)
val b2: Int => Int = sum(1, _, 3)

