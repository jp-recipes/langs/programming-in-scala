import javax.swing.JButton
// SAM (Single Abstract Method)
// A lambda expression can be used anywhere an instance of a class or interface contains just a SAM is required

val button = JButton()
button.addActionListener(
  _ => println("pressed!")
)

trait Increaser:
  def increase(i: Int): Int

def increaseOne(increaser: Increaser): Int =
  increaser.increase(1)

increaseOne(
  new Increaser:
    def increase(i: Int): Int = i + 7
)

increaseOne(i => i + 7)