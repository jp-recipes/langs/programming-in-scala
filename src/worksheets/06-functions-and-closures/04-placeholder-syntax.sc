val someNumbers = List(5, 10)

someNumbers.filter(_ > 0)
someNumbers.filter(x => x > 0)

// Insufficient type information for parameters
// val f = _ + _
val f = (_: Int) + (_:Int)
val g: (Int, Int) => Int = _ + _


