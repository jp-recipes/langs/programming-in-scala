val increase = (x: Int) => x + 1

val addTwo = (x: Int) =>
  val increment = 2
  x + increment