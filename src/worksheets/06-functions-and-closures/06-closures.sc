val c1 = (x: Int) => x + 1
val c2: Int => Int = x => x + 1
val c3: Int => Int = _ + 1

val someNumbers = List(-11, -10, -5, 0, 5, 10)
var sum = 0
someNumbers.foreach(sum += _)


def makeIncreaser(more: Int) = (x: Int) => x + more

val i1 = makeIncreaser(1)
val i2 = makeIncreaser(9999)