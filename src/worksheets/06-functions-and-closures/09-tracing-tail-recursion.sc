// Limitis of tail recrusion
// Scala only optimizes directly recursive calls back to the same function making the call

// This functions is not tail recursive, because performs an increment operation after the recursive call
def boom(x: Int): Int =
  if x == 0 then throw Exception("boom!")
  else boom(x - 1) + 1
// scala> boom(3)
// java.lang.Exception: boom!
//  at .boom(<console>:5)
//  at .boom(<console>:6)
//  at .boom(<console>:6)
//  at .boom(<console>:6)
//  at .<init>(<console>:6)
//  ...

// If we now modify boom so that it does become tail recursive
def bang(x: Int): Int =
  if x == 0 then throw Exception("bang!")
  else bang(x - 1)
// scala> bang(5)
//  at .bang(<console>:5)
//  at .<init>(<console>:6) ...