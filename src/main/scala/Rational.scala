package main

class Rational(n: Int, d: Int):
  require(d != 0) // will throw IllegalArgumentException

  private val g = gcd(n.abs, d.abs)
  val numer: Int = n / g
  val denom: Int = d / g

  def this(n: Int) = this(n, 1) // auxiliary constructor

  override def toString = s"$numer/$denom"

  def +(that: Rational): Rational =
    Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom
    )

  def -(that: Rational): Rational =
    Rational(
      numer * that.denom - that.numer * denom,
      denom * that.denom
    )

  def -(i: Int): Rational =
    Rational(numer - i * denom, denom)

  def *(that: Rational): Rational =
    Rational(numer * that.numer, denom * that.denom)

  def *(i: Int): Rational =
    Rational(numer * i, denom)

  def /(that: Rational): Rational =
    Rational(numer * that.denom, denom * that.numer)

  def /(i: Int): Rational =
    Rational(numer, denom * i)

  private def gcd(a: Int, b: Int): Int =
    if b == 0 then a else gcd(b, a % b)

extension (x: Int)
  def + (y: Rational) = Rational(x) + y
  def - (y: Rational) = Rational(x) - y
  def * (y: Rational) = Rational(x) * y
  def / (y: Rational) = Rational(x) / y